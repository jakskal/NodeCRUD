require('module-alias/register');
var { User } = require('@models');
const { response } = require('@helpers');
const crypt = require('bcrypt');

const userService = {

    get: async (req, res) => {
        try {
            const sort_order = req.query.priority
            if (req.query.priority) {
                const user = await User.all({
                    order: [
                        ['priority', sort_order]
                    ]
                })
                return res
                    .status(200)
                    .json(response(true, `User retrieved successfully sorted by priority ${sort_order}`, user, null));

            } else {
                const user = await User.all()
                return res.json(response(true, 'User retrieved successfully', user, null))
            }
        } catch (error) {
            if (error.errors) {
                return res.status(400).json(response(false, error.errors));
            }
            return res.status(400).json(response(false, error.message));
        }
    },
    /**
   * Save user to database
   *
   */
    create: async (req, res) => {
        const { name, priority, location, time_start, username, password } = req.body;
        try {
            const hashPassword = crypt.hashSync(password, 15);
            const payload = Object.assign(
                {},
                {
                    name,
                    priority,
                    location,
                    time_start,
                    username,
                    password:hashPassword,
                }
            );
            let users = await User.findOne({ where: { username: username } });
            if (users) {
                return res.json(response(false, `username ${username} already exist`))
            } else {
                users = await User.create(payload);
                return res
                    .json(response(true, 'User registered successfully', users));
            }
        } catch (error) {
            if (error.errors) {
                return res.status(400).json(error.errors);
            }
            return res.status(400).json(error.message);
        }
    },
    update: async (req, res) => {
        const data = req.body;
        let users = await User.findOne({ where: { username: data.username } })
        if (users) {
            const updated = await User.update(data, { where: { username: data.username } })
            users = await User.findOne({ where: { username: data.username } })
            return res.json(response(true, 'User updated successfully', users))
        } else {
            return res.json('username not exist')
        }
    },
    remove: async (req, res) => {
        const userId = req.params.id;

        const users = await User.destroy({ where: { id: userId } });
        if (users === 0) {
            return res
                .status(400)
                .json(response(false, `User with id ${userId} not found`));
        }
        return res.json(response(true, `user with id ${userId} success deleted`));

    }
}

module.exports = userService