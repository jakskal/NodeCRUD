# Node.JS CRUD

---

## About

This project uses [Node](https://nodejs.org/).

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1.  Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
    Required version:
    ```
    "node": "^9.0.0",
    ```
2.  Install your dependencies

    ```
    cd path/to/nodeCRUD; npm install
    ```

3.  Set `config/config.json` file as your database connection

    ### Create .env file in root directory of the project

    Write this in `config.json` file

    > config/config.json

    ```
    "username": "your database username",
    "password": "your database password",
    "database": "your database name",
    ```

4.  Migrate database using sequelize
    Run these commands in order

    ```
    node_modules/.bin/sequelize db:create
    ```

    ```
    node_modules/.bin/sequelize db:migrate
    ```

5.  Start your app

    ```
    npm start
    ```

## Route List

### User
#### GET User, method : ''GET"
```
GET /users
```
#### Query Parameter
Name | Type  | Required/Optional | Description
--- | --- | ---  | ---  | ---
priority | string | optional  | Available values are `ASC, DESC`.
#### Example
```
GET /api/users?priority=ASC
```
#### Create User, method : "POST"
#### Request Body
Name | Type  | Required/Optional | Description
--- | --- | ---  | ---  | ---
name | String | Required  | 4-500 character
priority | Integer | Required  | 0-2 character
location  |  String  |  Required  | 0-273 character
time_start| Time | Required | YYYY-MM-DDTHours:minutes:seconds
username| String | Required | 0-100 character
password| String | Required | 5-20 Character


#### Example
```
POST /users
```
#### Update User, method : "PATCH"
#### Request Body
Name | Type  | Required/Optional | Description
--- | --- | ---  | ---  | ---
name | String | Optional  | 4-500 character
priority | Integer | Optional  | 0-2 character
location  |  String  |  Optional  | 0-273 character
time_start| Time | Optional | YYYY-MM-DDTHours:minutes:seconds
username| String | `Required` | 0-100 character
password| String | Optional | 5-20 Character
#### Example
```
PATCH /users
```
#### Delete User, method : "DELETE"
delete user based on id
#### Example
```
DELETE /users/{id}
```
