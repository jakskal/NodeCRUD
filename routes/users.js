require('module-alias/register')
var express = require('express');
var router = express.Router();
var { User: user } = require('@models')
const { userService } = require('@services')
const { check, body, validationResult } = require('express-validator/check');
const { response } = require('@helpers');
/* GET users listing. */

router.post('/',
  [
    check('name', 'Name should be present')
      .exists()
      .matches(/^[A-Za-z\s]+$/g)
      .withMessage('Name can only contain char and space')
      .isLength({
        min: 4
      }),
    check('priority', 'Priority should be present')
      .exists()
      .matches(/^([1-9])|^([1-9][0-9])+$/i)
      .withMessage('Only number in range 0-99 that allowed'),
    check('location', 'Location should be present')
      .exists(),
    check('time_start')
      .isISO8601()
      .withMessage('Birthday format should be YYYY-MM-DDThours:minutes:second'),
    check('password', 'Password should be present')
      .exists()
      .isLength({
        min: 5
      })
      .withMessage('Password must be at least 5 char long')
      .isLength({
        max: 20
      })
      .withMessage('Password must be not more than 20 char long')
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json(response(false, errors.array()));
    }
    userService.create(req, res);
  });

router.get('/', (req, res) => {
  userService.get(req, res);
});


router.patch('/',
  [
    check('username', 'username should be present')
      .exists()
  ],
  (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(422).json(response(false, errors.array()));
    }
    userService.update(req, res);
  })
router.delete('/:id', (req, res) => {

  userService.remove(req, res);
});

module.exports = router;
